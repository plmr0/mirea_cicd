#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <bits/stdc++.h>

std::string getEnv(const char* name, const char* defaultValue)
{
    char* lang = getenv(name);

    if (lang == nullptr)
        return defaultValue;
    else
        return lang;
}

int main(int argc, char** argv)
{
    std::vector<std::string> unique;

    std::stringstream args(getEnv("ARGS", "0"));
    std::string word;

    std::ofstream out;
    out.open("unique.txt");

    while (args >> word)
    {
        if (std::find(unique.begin(), unique.end(), word) == unique.end())
        {
            unique.emplace_back(word);
            if (out.is_open())
                out << word << " ";
        }
    }

    out.close();

    return 0;
}
